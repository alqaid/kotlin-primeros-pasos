/*

crear firebase file:
 https://firebase.google.com/docs/cloud-messaging/android/receive?hl=es

Leer:
https://developer.android.com/guide/topics/ui/notifiers/notifications



https://developer.android.com/training/notify-user/build-notification


PRIMERO ES REGISTRAR UN CANAL DE NOTIFICACIONES, OBLIGATORIO DESDE ANDROID.8
    Debe ser llamado tan pronto como se ejecute la app
     Se considera que realizar llamadas repetidas es seguro porque cuando se crea un canal de notificación no se lleva a cabo ninguna operación.


    private fun createNotificationChannel() {
            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val name = getString(R.string.channel_name)
                val descriptionText = getString(R.string.channel_description)
                val importance = NotificationManager.IMPORTANCE_DEFAULT
                val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                    description = descriptionText
                }
                // Register the channel with the system
                val notificationManager: NotificationManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.createNotificationChannel(channel)
            }
        }



COMO CREAR LA NOTIFICACIÓN:
    https://developer.android.com/training/notify-user/build-notification

    BÁSICA:
         var builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.notification_icon)
            .setContentTitle(textTitle)
            .setContentText(textContent)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

    CON TEXTO LARGO:    
         var builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.notification_icon)
            .setContentTitle("My notification")
            .setContentText("Much longer text that cannot fit one line...")
            .setStyle(NotificationCompat.BigTextStyle()
                    .bigText("Much longer text that cannot fit one line..."))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)


COMO HACER APARECER LA NOTIFICACION
    with(NotificationManagerCompat.from(this)) {
        // notificationId is a unique int for each notification that you must define
        notify(notificationId, builder.build())
    }





Cómo establecer la acción de toque de la notificación

Cada notificación debería responder a un toque, en general para abrir una actividad en la app que se corresponda con la notificación. Para hacerlo, debes especificar un intent de contenido definido con un objeto PendingIntent y transfiérelo a setContentIntent().

En el siguiente fragmento, se muestra cómo crear un intent básico para abrir una actividad cuando el usuario presiona la notificación:

         
            val intent = Intent(this, ActivityResultadoNotificacion::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

            val builder = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.notification_icon)
                    .setContentTitle("My notification")
                    .setContentText("Hello World!")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)



COMO RECOGER DATOS DEL DATA DE LA NOTIFICACIÓN

In FCM you received RemoteMessage instead of Bundle.
Below is the way I used in my application where data is my RemoteMessage

int questionId = Integer.parseInt(data.get("questionId").toString());
String questionTitle = data.get("questionTitle").toString();
String userDisplayName = data.get("userDisplayName").toString();
String commentText = data.get("latestComment").toString();

Below is my notification data which I am sending it from server

{
  "registration_ids": "",
  "data": {
    "questionId": 1,
    "userDisplayName": "Test",
    "questionTitle": "Test",
    "latestComment": "Test"
  }
}










*/
