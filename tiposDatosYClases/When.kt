fun sentenciasWhen(){
// ver más https://play.kotlinlang.org/byExample/02_control_flow/01_When

	val country = "España"
	
	when(country){
	    	"España", "Mexico", "Colombia" -> {
				println("El Habla es Español")
		}	"Francia" -> {
				println("El pais es Frances")
		}	"EEUU" -> {
				println("El pais es Ingles")
		}	else -> {
				println("El pais es Español")
		}
	}
	
	val age =10 
	
	when (age){
		0 , 1 , 2 -> {
			println("Eres un bebé")
		}  5, 6 -> {  
			println("Eres un niño")
		}  in 7 .. 20 -> {
			println( "Eres un adoslescente")
		} else ->{
			println( "Eres un vegete")
		}
	}
}
