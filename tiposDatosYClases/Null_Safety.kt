fun kotlinNullSafety(){
//Seguridad contra Nulos NULL SAFETY 
/*
Al intentar meter un null en una variable directamente devolverá error de compilación
        var intento = null   --- Error de compilación
     

agregamos una variable con eltipo String?
*/
        var myString2 : String? = "valor"
		myString2= null   // metemos null en un momento dado en la función
        println(myString2)     ///  devuelve null ya no hay fallo de compilación


        // solucion elegante  SOLO EJECUTA Lo que hay entre llaves si myString2 no es nulo
        myString2?.let {
            println("Entró por aquí")
            println(it)
        }
}