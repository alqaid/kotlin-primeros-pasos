fun kotlinDiccionariosMapas{
// Diccionarios o también llamados mapas 

    // definimos:    tipo de dato clave y tipo de dato del valor
    // ojo CLAVES ÚNICAS
   var myMap: Map<Int, String>

    	myMap =  mapOf(1 to "Brasil", 2 to "España", 3 to "Francia")
    	println(myMap)

    	// si asignamos de nuevo eliminamos lo anterior
    	myMap = mapOf(4 to "Brasil", 5 to "España", 6 to "Francia")
    	println(myMap)
}


fun kotlinDiccionariosMutables{

// Diccionarios MUTABLES
var myMap2  = mutableMapOf(1 to "Brasil", 2 to "España", 3 to "Francia")
    	println(myMap2)

    	myMap2[6] = "Portugal"
    	myMap2.put(5, "Italia")
    	myMap2.put(3,  "Alemania")   // OJO reescribimos la 3
    	println(myMap2)

//Acceso a un dato
    	println("Valor clave 2: " + myMap2[2])

// Eliminar un dato
    	myMap2.remove(6)
    	println(myMap2)


/*
RESULTADO:
 	{1=Brasil, 2=España, 3=Francia}
	{1=Brasil, 2=España, 3=Alemania, 6=Portugal, 5=Italia}
	Valor clave 2: España
	{1=Brasil, 2=España, 3=Alemania, 5=Italia}*/
}