fun tratamientoArrays(){ 

		val nombre1= "Angel"   // constantes
    	val nombre2= "Pepe"
    	val nombre3= "David"

 Definir un Array
    	Estructura ordenada

// definir de golpe:
    	val myArray0 = listOf("Albacete","Chinchilla","Madrigueras")

//definir paso a paso
    	val myArray  = arrayListOf<String>()

    	// añadir datos de uno en uno
    	myArray.add(nombre1)
    	myArray.add(nombre2)
    	myArray.add(nombre3)
    	myArray.add(nombre3)

    	println(myArray)

// añadir en conjunto  con ListOf
    	myArray.addAll(listOf("Juanito","Manolo"))
    	println(myArray)


    // acceso a datos del array
    	val myNombre : String = myArray[0]

    	println("Posición 0 "+ myNombre)
    	println("Posición 3 " + myArray[3])

    // modificación de datos
    	println(myArray)
    	myArray[3] = "Fran"
    	println("Posición 3 " + myArray[3])
    	println(myArray)
    	// eliminar datos del array
    	myArray.removeAt( 3)
    	println("Posición 3 " + myArray[3])
    	println(myArray)
// recorrer un array con For
    	myArray.forEach (){
        		println("Elemento: " + it)
    	}

// array contar
  	println("total valores: " +  myArray.count())

// array Primer y ultimo elemento
    	println(  myArray.first())
    	println(myArray.last())

// array ordenar
    	myArray.sort()
    	//  no se usar:	myArray.sortBy {  }
    	println("Tras ordenar: " + myArray)

// array borrar todo
    	myArray.clear()
    	println("Tras borrar: " + myArray)
		
		}