fun kotlinBucles(){

///Bucle for
val myArraynumerico=(0..20)
        for (myNum in myArraynumerico) {
            println(myNum)
        }

        val myArray = listOf("Albacete","Chinchilla","Madrigueras")
        var myMap =  mutableMapOf(1 to "Brasil", 2 to "España", 3 to "Francia")

        for (myString in myArray) {
            println("Mi valor es ${myString}")
        }

        for (myElement  in myMap){
            println("Mi elemento:"  + myElement.key + " Valor:" + myElement.value)
        }

/*
RESULTADO:
Mi valor es Albacete
	Mi valor es Chinchilla
	Mi valor es Madrigueras
I/System.out: Mi elemento:1 Valor:Brasil
	Mi elemento:2 Valor:España
	Mi elemento:3 Valor:Francia
	*/
	
	
//Bucle while

var x = 0
while (x<10){
	println(x)
	x+=1
}
}