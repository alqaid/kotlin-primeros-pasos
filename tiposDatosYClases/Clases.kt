fun kotlinCreacionClases(){

		val sara = Programmer("Sara", 35, arrayOf(Programmer.Language.JAVA), arrayOf(brais))
        sara.code()

        println("${sara.friends?.first()?.name} es amigo de ${sara.name}")

	var pedro = agentes("Pedro","García Romero","21.569.437-S","Ana Karenina",null,"Albacete","02001",agentes.Idiomas.Gallego)

}

class agentes ( var nombre: String,
                var apellidos: String,
                var dni: String,
                var domicilio: String,
                var edad: Int? = null,
                var poblacion: String? = null,
                var cp: String? = null,
                var idioma: Idiomas ? = agentes.Idiomas.Castellano) {

    enum class Idiomas {
        Castellano,
        Catalan,
        Euskera,
        Gallego
    }

    fun suDominicilio(): String{
        var devolver : String = domicilio
        if (poblacion !=null) {
            devolver = devolver + " " + poblacion
        }
        if (cp !=null) {
            devolver = devolver + " (" + cp + ")"
        }
        return devolver
    }
}


class Programmer(val name: String,
                 var age: Int,
                 val languages: Array<Language>,
                 val friends: Array<Programmer>? = null) {

    enum class Language {
        KOTLIN,
        SWIFT,
        JAVA,
        JAVASCRIPT
    }

    fun code() {
        for (language in languages) {
            println("Estoy programando en $language")
        }
    }

}
