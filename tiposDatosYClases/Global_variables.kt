// Creamos una nueva Kotlin file/Class


object global {
    var usuario= "YO"
    var registrado ="NO"
}




// Accedemos  desde toda la App  para leer o modificar:
 
    global.usuario="user"

    println(global.usuario)


// CREAR array global de OBJETOS y acceder a el incluso y comprobar si está declarado.


 //--------- Creamos como var: 
                object global {
                        var variosNodos:  Array<nodoauzalan>? = null
                }


 //--------- Accedemos
                if (global.variosNodos  == null) {
                    val gson = Gson()
                    val misNodos:  Array<nodoauzalan> = gson.fromJson(result ,Array<nodoauzalan> ::class.java)
                    
                    // inicializamos de otro.
                    global.variosNodos=misNodos

                    // accedemos a él preguntando incluso si null
                    Log.d("RESULTADO onCreate", "un glob: ${global.variosNodos!![0].Titulo}")
                }
