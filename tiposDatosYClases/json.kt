  
// USAMOS GOOGLE/GSON-  https://github.com/google/gsonG


//declaro dependencias en build.gradel
  implementation 'com.google.code.gson:gson:2.8.5'


// declaro gson e importo clase

        val gson = Gson()

                // Parsear una clase en json
                val persona = agentes("Angel","Alcaide Romero","565656565x", "calle Maria Karey",44,"Albacete","02006",agentes.Idiomas.Castellano)
                val json:String= gson.toJson(persona)

                // recoger un json en una clase
                val vString1:String= json
                val persona2:agentes=gson.fromJson(json,agentes::class.java)


                Log.d( "RESULTADO",json)
                Log.d( "RESULTADO",persona2.nombre + persona2.apellidos)


// Array de json
        val gson = Gson()
        val unNodo:  Array<nodoauzalan> = gson.fromJson(result, Array<nodoauzalan>::class.java)
        
        Log.d("RESULTADO 3", "Nodo títlo: ${unNodo[0].Titulo}")



// LIST de json
          val gson = Gson()
          val misNodos: List<nodoauzalan> = gson.fromJson(result , Array<nodoauzalan>::class.java).toList()
        
            Log.d("RESULTADO 3", "Nodo títlo: ${misNodos[0].Titulo}")
