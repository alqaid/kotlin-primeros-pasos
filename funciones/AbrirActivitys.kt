
// Función Abrir una nueva actividad y borrar actual

            val intent = Intent(this, MainActivity()::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)      // opcional, borrar activiti actual. ojo al volver 
            startActivity(intent)


// Función Abrir una nueva actividad ya creada y pasamos Parámetros: Parametro1

private fun openActivity_segunda(){
        println()
        println(" ----Acbrir actividad segunda-----------------------------  ")

        val texto : String = "Vengo de la primera activida"
        val intent = Intent(this, segunda()::class.java)
        intent.putExtra("Parametro1",texto)
        startActivity(intent)

    }


// Como en el onCreate recoger en la nueva actividad el parámetro pasado:


        val intent = getIntent()
        val extras = getIntent().extras
        val s = extras!!.getString("Parametro1")   // si viene Null NO DA ERROR
        textView1.text=s
