//  Función reescrita para imprimir por pantalla 


// Creamos un nuevo fichero de values
// Derecho en res.values -> new / resource values file
// nombre del fichero strings.xml y directorio values-es

// En ingles por defecto ,  copiamos y pegamos codigo
<resources>
    <string name="app_name">App2Kotlin</string>
    <string name="g_alertAlerta">Alert!</string>
    <string name="g_alertInformacion">Information</string>
</resources>


// En castellano
<resources>
    <string name="app_name">App2Kotlin</string>
    <string name="g_alertAlerta">Alerta!</string>
    <string name="g_alertInformacion">Información</string>
</resources>



// Acceso a estas variables por idioma es simpre:
    getString(R.string.g_alertInformacion)   // desde codigo kotlin
    @strings.g_alertInforacion              //  desde layouts xml


