// se ha de trabajar desde un principio con androidx

/*
    https://firebase.google.com/brand-guidelines   para coger sus logos y colores

*/


/*
    Integrar Firebase

0 ->   El proyecto debe soportar Androidx 
 

    1 f->  En firebase Agregar App  y marcar tipo Android
    2 f->  Nombre paquete Android. En Android Manifest, package: com.example.app2kotlin
    3 f->  Escribir un nombre o apodo para firebase: Mi app 1.
    4 f->  Registrar App
    5  As-> Descargar google-services.json
    6  As-> Instalar, cambiamos a vista proyecto, descargamos el fichero  en la jerarquia de app
    7 f->  En Firebase de nuevo, damos a siguiente y seguimos el tutorial de firebase:

    8  As-> Agregar en builgradel a nivel de PROYECT 3 lineas de código:
                google() en repositories de buildscript  <---  este suele estar.
                classpath 'com.google.gms:google-services:4.3.3'  en dependencias
                google() en repositories de allproyect  <---  este suele estar.

    9  As-> Agregar en builgradel a nivel de APP 3 lineas de código:      
            EN LA ZONA SUPERIOR 2 lineas si no están:
                apply plugin: 'com.android.application'  <---  este suele estar.
                apply plugin: 'com.google.gms.google-services'    
            En dependencias:
                implementation 'com.google.firebase:firebase-analytics:17.2.2'
    10  As-> Sincronizar

    11 f->  Volvemos a firebase y pulsar en siguiente
    12 f->  Lanzamos de nuevo la aplicación para que se sincronize


Fallos posibles, error de androidx:

        Androidx en caso de tener que rehacer mucho codigo 
        todo por quitar la dependencia:   //  implementation 'com.android.support:appcompat-v7:28.0.0'
        por la dependencia:                   implementation 'androidx.appcompat:appcompat:1.0.0'

        y agregar en Gradle properties:
            android.useAndroidX=true
            android.enableJetifier=true
 

            renombrar
*/
