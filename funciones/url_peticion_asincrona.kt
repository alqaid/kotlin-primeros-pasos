// implementacion
   implementation "org.jetbrains.anko:anko:0.10.4"


// RECOGEMOS DE FORMA ASINCRONA LA URL


val url2 :String="https://httpbin.org/get"
                doAsync{
                    val result = URL(url2).readText()

                    // USAMOS ESTA EXCEPCION DE LA FORMA ASINCRONA PARA MOSTRAR TOAST
                    uiThread {
                        toast(result)
                        Log.d("RESULTADO", result)
                    }
                }
