

// Primero creamos en res.drawable  un nuevo: Drawable Resorce Files
// Es decir un diseño personalizado o plantilla para luego aplicar a los activitis
//  Fichero se llama por ejemplo: splashscreen

    <?xml version="1.0" encoding="utf-8"?>
    <layer-list xmlns:android="http://schemas.android.com/apk/res/android">
        <item android:drawable="@color/colorPrimary" />
        <item>
            <bitmap android:src="@mipmap/logo1"
                android:gravity="center"/>
        </item>
    </layer-list>
// importante en lugar de @mipmap/ic_launcher podemos poner cualquier imagen, 
// pero previamente prepararla para todas las resoluciones posibles

/* ---------------------------------------------------------------------------------- */
// En res.values editamos syles.xml 
//      Creamos un stilo predefinido para mi splashscreen  
//      tomamos heredado el tema principal AppTheme 
//      Y AÑADIMOS UN DISEÑO O PRESENTACIÓN  nuestro xml diseñado splashscreen

     <style name="SplashTheme" parent="AppTheme">
        <item name="android:windowBackground">@drawable/splashscreen</item>
    </style>

/* ---------------------------------------------------------------------------------- */
// Ahora vamos a AndroidManifest.xml
// En una activity ahora REEMPLAZAMOS el tema de esta por el nuevo stilo creado: SplashTheme
// Esa activity será la activity principal la que se carga al abrir la app MainActivity

   Reemplazamos: <activity android:name=".MainActivity">
            por: <activity android:name=".MainActivity" android:theme="@style/SplashTheme">

/* ---------------------------------------------------------------------------------- */
// Finalmente en MainActivity.kt
// Redefinimos el estilo a cargar, por defecto de AndroidManifest biene cargado SplashTheme
// Volvemos a poner su estilo original:
        
        //1ª linea y antes de super.oncreate    
        //Definir el estilo del diseño
       setTheme((R.style.AppTheme)) 
    
