
//https://developer.android.com/guide/topics/ui/look-and-feel/fonts-in-xml

// creamos una carpeta desde app/RES:   USAMOS LA OPCION-->> new | folder | font-resources-folder

// Se pegan en la carpeta las nuevas fuentes, desde: show files


// Creamos los estilos asociados a las fuentes para que se pueda acceder desde toda la app:
// De nuevo  desde la carpeta fonts:   new |  font-resorce-file

<?xml version="1.0" encoding="utf-8"?>
<font-family xmlns:android="http://schemas.android.com/apk/res/android">
    <font
        android:fontStyle="normal"
        android:fontWeight="400"
        android:font="@font/josefinsans_regular" />
</font-family>






// Desde  onCreate de la Activity

        val typeface0 = resources.getFont(R.font.josefinsans_regular)
        textTitle.typeface = typeface0


//----------------------------------------------------------------------------
//       segunda parte como crear una familia y aplicarla en toda la app

         <item name="android:fontFamily">@font/josefinsans_regular</item>

//----------------------------------------------------------------------------
//       tercera parte como reescribir un estilo de fuente para darle otra importancia:

    <style name="josefin2" parent="AppTheme">
            <item name="android:fontFamily">@font/josefinsans_bolditalic</item>
           <item name="android:textSize">15pt</item>
    </style>


// desde el xml de la activity:
    style="@style/josefin2"

//ojo, si el elemento textview o el que sea ya tiene un textsize ignora el de la plantilla
