//Archivo MANIFIESTO

   <uses-permission android:name="android.permission.INTERNET">10</uses-permission>
   
   
//Archivo kt:
    private val BASE_URL="https://www.alcaide.info"
    private val URL_GOOGLE="https://google.com/search?q="  // para Google

override fun onCreate(savedInstanceState: Bundle?) {
       super.onCreate(savedInstanceState)


        //Definir Vista del diseño
        setContentView(R.layout.activity_main)

        //  --------         SwipeRefresh  (permite refrescar la web)            ---------
        swipe.setOnRefreshListener {
            navegador.reload()
            // genera un problema al refrescar el puntero sigue dando vueltas. hay que cambiarlo en el webview  (*)1
        }
// ------------------------------  SEARCH FUNCIONES DE ESCRITURA SOBRE EL BUSCADOR searchView--------------
        buscador.setOnQueryTextListener ( object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                // delegar que el componente nos sugiera LE DECIMOS QUE NO
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                // ESTA FUNCION SE INVOCA CUANDO SE DA CLIC A BUSCAR

                query?.let {
                    // si el texto que introdujo el usuario es no nulo
                    if (URLUtil.isValidUrl(it)) {    // Funcion de Android para validar una URL
                        //ES UN URL
                        navegador.loadUrl(it)
                    } else {
                        //NO LO ES, vamos a google
                        navegador.loadUrl(("$URL_GOOGLE$it"))
                    }
                }
               return true
            }

        })

        //  --------         WEBVIEW            ---------

        //componentes de vista definimos los dos posibles siempre
        navegador.webChromeClient = object : WebChromeClient(){

        }

        navegador.webViewClient = object : WebViewClient(){


            //  (*)1  debemos sobreescribir estos metodos  para conseguir controlar el webview
            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                return false
            }

            //  (*)1  debemos sobreescribir ahora que si hay refresco muestre el puntero, se ha desactivado en la anterior
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)

                buscador.setQuery(url,false)  // reescribimos la url en el buscador una vez que se ha cargado
                swipe.isRefreshing=true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                swipe.isRefreshing=false
            }


        }

        //definimos javascript
        val settings = navegador.settings
        settings.javaScriptEnabled=true

        //lanzamos la página
        navegador.loadUrl(BASE_URL)

}


 override fun onBackPressed() {
        if (navegador.canGoBack()){
            // si el navegador puede volver atrás hacemos atras en la web, SINO, es la página la que vuelve a la otra Activity o Cierra la APP
            navegador.goBack()
        }else{
            //  ---> va hacia atras, sino hay más Activity CIERRA LA APP, No queda muy fino--> super.onBackPressed()
            // Reemplazo por Snackbar


        }
    }