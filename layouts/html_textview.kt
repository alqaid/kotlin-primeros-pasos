var contenido = global.variosNodos!![idNodo].Contenido
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    txtNodoContenido.text = Html.fromHtml(contenido, Html.FROM_HTML_MODE_COMPACT)
                } else {
                    txtNodoContenido.text = Html.fromHtml(contenido)
                }
